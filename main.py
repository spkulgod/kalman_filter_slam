import numpy as np
from utils import *
from scipy.linalg import expm
from scipy.linalg import block_diag
np.set_printoptions(threshold=np.nan)
import my_functions

def hat_3(a):
	a_hat = np.zeros((3,3))
	a_hat[0,1] = -a[2]
	a_hat[1,0] = a[2]
	a_hat[0,2] = a[1]
	a_hat[2,0] = -a[1]
	a_hat[1,2] = -a[0]
	a_hat[2,1] = a[0]
	return a_hat

def hat_4(a):
	aa = hat_3(a[3:])
	ab = np.array([a[:3]]).T
	ac = np.zeros((1,4))
	a_hat = np.append(np.append(aa,ab,axis=1),ac,axis=0)
	return a_hat

def adj(a):
	w = hat_3(a[3:])
	v = hat_3(a[:3])
	z = np.zeros((3,3))
	top = np.append(w,v,axis=1)
	bot = np.append(z,w,axis=1)
	a_adj = np.append(top,bot,axis=0)
	return a_adj

class Prediction:
	def __init__(self):
		state = np.identity(4)
		self.traj = np.array([state]).T
		self.noise = 0.00001*np.identity(6)
		self.sigma = 0.1*np.identity(6)
		self.state = np.identity(4)

	def add_to_traj(self,state):
		state = np.linalg.inv(state)
		state = np.array([state.T]).T
		self.traj = np.append(self.traj,state,axis=2)

	def next_state(self,zeta,tau,state,sigma_t):
		u_hat = hat_4(zeta)
		u_adj = adj(zeta)
		sigma_t = sigma_t[-6:,-6:]
		self.state = state
		self.state = np.dot(expm(-tau*u_hat),self.state)
		self.sigma = expm(-tau*u_adj)@sigma_t@expm(-tau*u_adj).T + \
				tau*tau*self.noise
		self.add_to_traj(self.state)

'''updates only the position of the landmarks and not that of the robot'''
class Update:
	def __init__(self,reading,cam_T_imu,K,b):
		m = np.shape(reading)[1]
		self.mu = np.append(np.zeros((3,m)),np.ones((1,m)),axis=0)
		self.sigma = np.identity(3*m)
		self.indices = np.arange(m) 
		z = np.zeros((1,3))
		self.D = np.append(np.identity(3),z,axis=0)
		self.oTi = cam_T_imu
		M1 = np.append(K[:2,:],K[:2,:],axis = 0)
		M2 = np.zeros([4,1])
		M2[2,0] = -K[0,0]*b
		self.M = np.append(M1,M2,axis=1)
		self.V = 15*np.identity(4)
		self.fsb = K[0,0]*b
		self.K = K
		self.list = np.array([])

	def update(self,T,features):
		vals = np.argwhere(features[0,:]>0)
		bool = features>1
		H = np.zeros((4*np.shape(vals)[0],np.shape(self.sigma)[1]))
		for i in range(np.shape(vals)[0]):
			mu = self.mu[:,vals[i,0]]
			if(np.any(mu == 0)):
				zs = self.fsb/(features[0,vals[i,0]]-features[2,vals[i,0]])
				cam1_ft = np.append(features[:2,vals[i,0]],np.ones(1))
				temp = zs*(np.linalg.inv(self.K)@cam1_ft)
				coord = np.append(temp,np.ones(1))
				self.mu[:,vals[i,0]] = np.linalg.inv(self.oTi@T)@coord
			mu = self.oTi@T@self.mu[:,vals[i,0]].T
			dpi = np.identity(4)
			temp = -mu/mu[2] + np.array((0,0,1,0))
			dpi[:,2]= temp
			dpi = dpi/mu[2]
			Hij = self.M@dpi@self.oTi@T@self.D
			H[4*i:4*i+4,3*vals[i,0]:3*vals[i,0]+3] = Hij
		mu = self.oTi@T@(self.mu[bool].reshape((4,-1)))
		features = features[bool].reshape((4,-1))
		z_hat = self.M@(mu/mu[2,:])
		'''ft = np.argwhere(vals[:,0]==14)
		print(ft)
		if (ft>=0):
			print(features[:,ft] - z_hat[:,ft])'''
		if(np.shape(vals)[0]>0):
			V = np.kron(np.identity(np.shape(vals)[0]),self.V)
			K = self.sigma@H.T@np.linalg.inv(H@self.sigma@H.T + V)
			self.sigma = (np.identity(np.shape(H)[1]) - K@H)@self.sigma
			temp = K@(np.reshape((features - z_hat).T,(-1,1)))
			self.mu = self.mu + self.D@temp.reshape(-1,3).T

'''updates the position of the landmarks and the state of the robot'''
class Update2:
	def __init__(self,reading,cam_T_imu,K,b):
		m = np.shape(reading)[1]
		self.mu = np.append(np.zeros((3,m)),np.ones((1,m)),axis=0)
		self.sigma = 1*np.identity(3*m+6)
		z = np.zeros((1,3))
		self.D = np.append(np.identity(3),z,axis=0)
		self.oTi = cam_T_imu
		M1 = np.append(K[:2,:],K[:2,:],axis = 0)
		M2 = np.zeros([4,1])
		M2[2,0] = -K[0,0]*b
		self.M = np.append(M1,M2,axis=1)
		self.V = 1000000000*np.identity(4)
		self.fsb = K[0,0]*b
		self.K = K
		self.state = np.identity(4)

	def update(self,T,sigma_old,features):
		self.state = T
		self.sigma[-6:,-6:] = sigma_old
		vals = np.argwhere(features[0,:]>0)
		bool = features>0
		H = np.zeros((4*np.shape(vals)[0],np.shape(self.sigma)[1]))
		for i in range(np.shape(vals)[0]):
			mu = self.mu[:,vals[i,0]]
			if(np.any(mu == 0)):
				zs = self.fsb/(features[0,vals[i,0]]-features[2,vals[i,0]])
				cam1_ft = np.append(features[:2,vals[i,0]],np.ones(1))
				temp = zs*(np.linalg.inv(self.K)@cam1_ft)
				coord = np.append(temp,np.ones(1))
				self.mu[:,vals[i,0]] = np.linalg.inv(self.oTi@T)@coord
			if(features[0,vals[i,0]]-features[2,vals[i,0]]>0):
				mu = self.oTi@T@self.mu[:,vals[i,0]].T
				dpi = np.identity(4)
				temp = -mu/mu[2] + np.array((0,0,1,0))
				dpi[:,2]= temp
				dpi = dpi/mu[2]
				Hij = self.M@dpi@self.oTi@T@self.D
				dot = T@self.mu[:,vals[i,0]].T
				dot = np.append(dot[3]*np.identity(3),-hat_3(dot[:3]),axis=1)
				dot = np.append(dot,np.zeros((1,6)),axis=0)
				Hij2 = self.M@dpi@self.oTi@dot
				H[4*i:4*i+4,3*vals[i,0]:3*vals[i,0]+3] = Hij
				H[4*i:4*i+4,-6:] = Hij2
		mu = self.oTi@T@(self.mu[bool].reshape((4,-1)))
		features = features[bool].reshape((4,-1))
		z_hat = self.M@(mu/mu[2,:])
		if(np.shape(vals)[0]>0):
			V = np.kron(np.identity(np.shape(vals)[0]),self.V)
			K = self.sigma@H.T@np.linalg.pinv(H@self.sigma@H.T + V)
			self.sigma = (np.identity(np.shape(H)[1]) - K@H)@self.sigma
			kz = K@(np.reshape((features - z_hat).T,(-1,1)))
			temp = kz[:-6]
			self.mu = self.mu + self.D@temp.reshape(-1,3).T
			temp = hat_4(kz[-6:].reshape(6))
			self.state = expm(temp)@T
			#self.state = T
			#self.sigma[-6:,-6:] = sigma_old

if __name__ == '__main__':
	dset = 42
	filename = "./data/00"+str(dset)+".npz"
	t,features,linear_velocity,rotational_velocity,K,b,cam_T_imu = load_data(filename)
	zeta = np.append(linear_velocity,rotational_velocity,axis=0)
	predict = Prediction()
	update = Update(features,cam_T_imu,K,b)
	update2 = Update2(features,cam_T_imu,K,b)
	for i in range(1,np.shape(t)[1]):
		# (a) IMU Localization via EKF Prediction
		print(i)
		predict.next_state(zeta[:,i],t[0,i]-t[0,i-1],update2.state,update2.sigma)
		
		# (b) Landmark Mapping via EKF Update
		#update.update(predict.state,features[:,:,i])
		# (c) Visual-Inertial SLAM (Extra Credit)
		update2.update(predict.state,predict.sigma,features[:,:,i])
		visualize_trajectory_2d(predict.traj,update2.mu,i,dset)

	# You can use the function below to visualize the robot pose over tsime
	visualize_trajectory_2d(predict.traj,update2.mu,i+100,dset,show_ori=True)


