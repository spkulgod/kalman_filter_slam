#### Description:

This project presents an approach of implementing visual-inertial simultaneous localization and mapping(SLAM) using the Extended Kalman Filter and Landmark Mapping. In this work, the IMU data of the robot is used to estimate its movement and it is assumed that the data association for landmarks is done externally by another program. Extended kalman filter is used for prediction and updating of the robots position and the landmark locations.

#### Contents:  

main.py - contains the main program.  
my_functions.py - contains the classes/functions required for the main program.  
utils.py - contians the utility functions provided.  

#### Results:

![Alt text](gifs/slam2-27.gif "gif for the parts a and b of the question for dataset 27")
![Alt text](gifs/slam2-20.gif "gif for the parts a and b of the question for dataset 20")
![Alt text](gifs/slam2-42.gif "gif for the parts a and b of the question for dataset 42")

